# git

## Table Of Contents

* [About](#about)
* [Role Defaults](#role-defaults)
* [License](#license)
* [Author](#author)

## About

> Role that compiles git from source and installs it

[Back to table of contents](#table-of-contents)

## Role Defaults

**Quicklist**: [git_version](#git_version)

### git_version

* *help*: version of git to install.
* *default*: ``2.29.2``

[Back to table of contents](#table-of-contents)

## License

license (MIT)

[Back to table of contents](#table-of-contents)

## Author

[Brad Stinson](https://www.bradthebuilder.me)

[Back to table of contents](#table-of-contents)
